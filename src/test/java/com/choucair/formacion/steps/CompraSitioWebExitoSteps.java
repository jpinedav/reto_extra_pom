package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.RealizarCompraPageObject;
import com.choucair.formacion.pageobjects.SeleccionTvPageObject;
import com.choucair.formacion.pageobjects.SitioCompraVirtualExitoPageObject;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;

public class CompraSitioWebExitoSteps extends PageObject{
	
	SeleccionTvPageObject seleccionTvPageObject;
	SitioCompraVirtualExitoPageObject sitioCompraVirtualExitoPageObject;
	RealizarCompraPageObject realizarCompraPageObject;
	
	@Step
	public void ingreso_SitioWeb() throws InterruptedException
	{
		sitioCompraVirtualExitoPageObject.open();
	}

	@Step
	public void seleccion_Producto(String Dato)
	{
		//seleccionTvPageObject.CerrarVentana();
		seleccionTvPageObject.IngresarDato(Dato);
		seleccionTvPageObject.SeleccionarCaracteristicas();
	}
	
	@Step
	public void validar_Compra()
	{
		realizarCompraPageObject.confirmarCompra();
	}
}
