package com.choucair.formacion.definition;

import com.choucair.formacion.steps.CompraSitioWebExitoSteps;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;

public class CompraSitioWebExitoDefinition {
	
	@Steps
	CompraSitioWebExitoSteps compraSitioWebExitoSteps;
	
	@Dado("^que quiero comprar un televisor para ver el mundial de futbol$")
	public void que_quiero_comprar_un_televisor_para_ver_el_mundial_de_futbol() throws InterruptedException {
		compraSitioWebExitoSteps.ingreso_SitioWeb();
	}
	
	@Cuando("^ingreso a la página virtual del éxito y selecciono el (.*) que más me gusta$")
	public void ingreso_a_la_página_virtual_del_éxito_y_selecciono_el_que_más_me_gusta(String ProductoBuscar) {
		compraSitioWebExitoSteps.seleccion_Producto(ProductoBuscar);
	}

	@Entonces("^realizo la compra para que sea enviado a mi casa\\.$")
	public void realizo_la_compra_para_que_sea_enviado_a_mi_casa(){
		
		compraSitioWebExitoSteps.validar_Compra();
	}
}
