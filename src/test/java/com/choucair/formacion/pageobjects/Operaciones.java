package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.PageObject;

public class Operaciones extends PageObject{
	
	
	public void ListarTvsConsola (WebElement lstTelevisores) 
	{
		List<WebElement> Lista = lstTelevisores.findElements(By.tagName("a"));
		
		for (int i = 0; i<Lista.size(); i++)
		{
			System.out.println(Lista.get(i).getText());
		}
	}
	
	
	public void TiempoEspera (int t) 
	{

		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
	


