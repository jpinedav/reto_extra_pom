package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class RealizarCompraPageObject extends PageObject{
	
	@FindBy (xpath="//*[@id=\'headerUserInfo\']/div[2]/a/span[1]")
	public WebElementFacade btnCarrito;
	
	@FindBy (xpath="//*[@id=\'summary-page\']/div[1]/h4/span")
	public WebElementFacade lblCantidadConfirmada;
	
	@FindBy (xpath="//*[@id=\"product-standard\"]/div/div[3]/div/div[2]/div/div/input")
	public WebElementFacade lblCantidadComprada;
	
	@FindBy (xpath="//*[@id=\"summary-page\"]/div[1]/h4")
	public WebElementFacade lblConfirmacion;
	
	public void confirmarCompra() 
	{
		
		btnCarrito.click();
		String strCantidadConfirmada = lblConfirmacion.getText();
		assertThat(strCantidadConfirmada, containsString("1 productos en tu carrito"));
		System.out.println(strCantidadConfirmada);
	}
}
