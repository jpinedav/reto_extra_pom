package com.choucair.formacion.pageobjects;

import org.openqa.selenium.Keys;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SeleccionTvPageObject extends PageObject{
	
	Operaciones operaciones;

	@FindBy (xpath="//*[@id=\'modal-coverage-address\']/div/div/div[1]/button/span")
	public WebElementFacade btnCerrarEmergente;
	
	@FindBy (id="tbxSearch")
	public WebElementFacade txtBuscar;
	
	@FindBy (id="btnSearch")
	public WebElementFacade btnBuscar;
	
	@FindBy (xpath="//*[@id=\'filterBy\']/div[2]/div[2]/ul/li[2]")
	public WebElementFacade btnTamano;
	
	@FindBy (xpath="//*[@id=\'filterBy\']/div[2]/div[2]/ul/li[1]/label/input")
	public WebElementFacade chkMarca;
	
	@FindBy (xpath="//*[@id=\'filterCategories\']/div[2]/ul/li[3]/a")
	public WebElementFacade btnCategoriaPantalla;
	
	@FindBy (xpath="//*[@id=\"filterPriceRange\"]/div[1]/h4/aa")
	public WebElementFacade lblPrecio;
	
	
	@FindBy (id="close-wisepop-123971")
	public WebElementFacade btnCerrarEmergente2;
		
	@FindBy (xpath="//*[@class=\"row product-list\"]")
	public WebElementFacade lstTelevisores;	
	
	@FindBy (xpath="//*[@id=\'prd0002747648749062\']/div/a")
	public WebElementFacade tvSelecionado;
	
	@FindBy (xpath="//*[@class=\"btn btn-warning btn-lg btn-block btn-add-cart\"]")
	public WebElementFacade lblAnadirCarrito;
	
	//----
	
	public void CerrarVentana () {
		btnCerrarEmergente.click();
	}

	public void IngresarDato (String strDato) {
		txtBuscar.sendKeys(strDato);
		txtBuscar.sendKeys(Keys.ENTER);
		
//		try {
//			Robot m = new Robot();
//			m.mouseWheel(3);
//			
//			
//		} catch (AWTException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}	
		
	public void SeleccionarCaracteristicas (){
		
		btnTamano.click();
		btnCerrarEmergente2.click();
		operaciones.TiempoEspera(2000);
		withAction().moveToElement(btnCategoriaPantalla);
		chkMarca.click();
		btnCategoriaPantalla.click();
		operaciones.ListarTvsConsola(lstTelevisores);
		operaciones.TiempoEspera(3000);
		tvSelecionado.click();
		lblAnadirCarrito.click();
	}

}
