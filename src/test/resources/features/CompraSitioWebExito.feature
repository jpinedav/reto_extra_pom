#Author: jpinedav@choucairtesting.com
#language:es

@Regresion
Característica: Compra en sitio virtual del exito
El usuario quiere comprar un televisor por la página virtual del exito. 
Se tiene presente la marca, resolución y el tamaño del Tv. 

  @CasoExitoso
  Escenario: Seleccion y compra de televisor por la página virtual del exito. 
    Dado que quiero comprar un televisor para ver el mundial de futbol
    Cuando ingreso a la página virtual del éxito y selecciono el televisor que más me gusta
    Entonces realizo la compra para que sea enviado a mi casa.
